--SELECT json_agg("user") FROM "user" LIMIT 1000;
select * from user_role

--select json_build_array(
--  a.id,
--  a.username,
--  a.name)
--from "user" a
--order by a.id

select json_agg(struct) as "user"
from
(
  select
  a."id",
  a."username",
  a."name"
  json_agg(b) as user_roles
  from "user" a
  join (select user_id, role_id, enabled from user_role) b on b.user_id = a.id
) struct
