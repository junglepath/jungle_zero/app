require 'jungle/auth/default/password_hash'

module App
  module ZData
    Data = Jungle::Db::Model::Table::Data
    PWHash = Jungle::Auth::Default::PasswordHash

    def self.bootstrap(db:, models:, password: 'test')
      insert_users(db, models, password)
      insert_roles(db, models)
      insert_user_roles(db, models)
      #insert_dummy_contacts(db, models)
    end

    private

    def self.insert_users(db, models, password)
      hash = PWHash.create_hash(password)
      users = [
        {id: 0, name: 'root', username: 'root', default_role: 'root', password_hash: hash, email: 'root@jungle_zero', enabled: true, created_by_user_id: 0, updated_by_user_id: 0},
        {id: 1, name: 'admin', username: 'admin', default_role: 'admin', password_hash: hash, email: nil, enabled: true, created_by_user_id: 0, updated_by_user_id: 0},
        {id: 2, name: 'user', username: 'user', default_role: 'user', password_hash: hash, email: nil, enabled: true, created_by_user_id: 0, updated_by_user_id: 0}
      ]
      do_inserts(db, models, :user, users)
      fix_serial(db, :user)
    end
    
    def self.insert_roles(db, models)
      roles = [
        {id: 0, role: 'root', description: 'Can do anything.', enabled: true, created_by_user_id: 0, updated_by_user_id: 0},
        {id: 1, role: 'admin', description: 'Can do administrative functions.', enabled: true, created_by_user_id: 0, updated_by_user_id: 0},
        {id: 2, role: 'user', description: 'Can do user functions.', enabled: true, created_by_user_id: 0, updated_by_user_id: 0},
      ]
      do_inserts(db, models, :role, roles)
      fix_serial(db, :role)
    end

    def self.insert_user_roles(db, models)
      user_roles = [
        {user_id: 0, role_id: 0, enabled: true, created_by_user_id: 0, updated_by_user_id: 0},
        {user_id: 0, role_id: 1, enabled: true, created_by_user_id: 0, updated_by_user_id: 0},
        {user_id: 0, role_id: 2, enabled: true, created_by_user_id: 0, updated_by_user_id: 0},
        {user_id: 1, role_id: 1, enabled: true, created_by_user_id: 0, updated_by_user_id: 0},
        {user_id: 1, role_id: 2, enabled: true, created_by_user_id: 0, updated_by_user_id: 0},
        {user_id: 2, role_id: 2, enabled: true, created_by_user_id: 0, updated_by_user_id: 0}
      ]
      do_inserts(db, models, :user_role, user_roles)
    end

    def self.insert_dummy_contacts(db, models)
      contacts = [
        {id: 0, name: 'me', phone: '111111'},
        {id: 1, name: 'you', phone: '222222'}
      ]
      do_inserts(db, models, :contact, contacts)
      fix_serial(db, :contact)
    end

    def self.do_inserts(db, models, key, array)
      array.each do |hash|
        data = Data.new(hash, models[key], true, true, false)
        result = db.insert._model(data)
        puts "result: #{result.to_h}"
      end
    end

    def self.fix_serial(db, key)
      db.reset_sequence_for_table(key.to_s)
    end
  end
end
