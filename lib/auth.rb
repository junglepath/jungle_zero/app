require 'rack'
require 'rack/auth/abstract/handler'
require 'rack/auth/abstract/request'

module Lib
  module Auth
    def self.basic(env)
      auth = Request.new(env)
      if auth.provided? and auth.basic?
        env['REMOTE_USER'] = auth.username
        env['REMOTE_PASSWORD'] = auth.password
      else
        env['REMOTE_USER'] = nil
        env['REMOTE_PASSWORD'] = nil
      end
      puts "credentials: #{env['REMOTE_USER']}:#{env['REMOTE_PASSWORD']}"
      env
    end

    private
    
    class Request < ::Rack::Auth::AbstractRequest
      def basic?
        "basic" == scheme
      end

      def credentials
        @credentials ||= params.unpack("m*").first.split(/:/, 2)
      end

      def username
        credentials.first
      end

      def password
        credentials.last
      end
    end
  end
end
