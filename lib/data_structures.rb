require 'oj'

module Lib
  module DataStructures
    #
    # Functions to convert / dump / format / display (puts)
    # json strings / hashes / arrays so I can just call these
    # and not have to remember the proper options, etc.
    # And to ensure that I don't end up dumping json strings
    # that have keys like: ":my_key_what_is_the_colon_doing_in_here?"
    # Because they will then load into a symbolized hash key with the
    # key symbol also containing a ":" character. :(
    #
    class Error < StandardError; end
    class InvalidArgumentError < Error; end

    def self.json_symbolize(object)
      return nil unless object
      options = {symbol_keys: true}
      return Oj.load(Oj.dump(object, options), options) if object.class == ::Hash
      return Oj.load(Oj.dump(object, options), options) if object.class == ::Array
      return Oj.load(object, options) if object.class == ::String
      raise InvalidArgumentError, "object.class: #{object.class}."
    end

    def self.json_load(object)
      return nil unless object
      options = {symbol_keys: false}
      return Oj.load(Oj.dump(object, options), options) if object.class == ::Hash
      return Oj.load(Oj.dump(object, options), options) if object.class == ::Array
      return Oj.load(object, options) if object.class == ::String
      raise InvalidArgumentError, "object.class: #{object.class}."
    end

    def self.json_dump(object)
      return nil if object == nil
      options = {symbol_keys: true}
      return Oj.dump(object, options) if object.class == ::Hash
      return Oj.dump(object, options) if object.class == ::Array
      return Oj.dump(Oj.load(object, options), options) if object.class == ::String
      raise InvalidArgumentError, "object.class: #{object.class}."
    end

    def self.json_dump_formatted(object)
      o = json_load(object)
      return puts nil if object == nil
      options = {symbol_keys: true, indent: 2}
      return Oj.dump(o, options) if o.class == ::Hash
      return Oj.dump(o, options) if o.class == ::Array
      return Oj.dump(Oj.load(object, options), options) if o.class == ::String
      raise InvalidArgumentError, "object.class: #{object.class}."
    end

    def self.json_puts(object, title: nil)
      puts title if title
      puts json_dump_formatted(object)
    end
  end
end

