module Env
  SEnv = Struct.new(:req, :identity, :params, keyword_init: true)
  def self.prep(req)
    req = Lib::Auth.basic(req)
    identity = App.authenticate(req['REMOTE_USER'], req['REMOTE_PASSWORD'])
    params = {}
    env = SEnv.new(req: req, identity: identity, params: params)
    puts "--- req ---"
    env.req.each do |k, v|
      puts "#{k}: #{v}."
    end
    puts "--- --- ---"
    puts "identity: #{env.identity}."
    puts "params: #{env.params}."
    env
  end
end
