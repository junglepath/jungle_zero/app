require_relative './app'
require_relative '../lib/env'

module App
  module Api
    def self.default(req)
      env = Env.prep(req)
      #[301, {'Location' => '/index.html'}, []]
      #[200, {}, [File.read(File.join(App.public_dir, 'index.html'))]]
      [200, {}, ['Test']]
    end

    def self.app(req)
      env = Env.prep(req)
      [200, {}, ["aaa\n", env.req['PATH_INFO']]]
    end

    def self.contacts(req)
      env = Env.prep(req)
      ds = App.db.base['select * from contact']
      data = ds.all
      pp data
      [200, {'Content-Type' => 'application/json'}, [data.to_json]]
    end
  end
end
