require 'agoo'
require_relative './api'
require_relative './app'
require_relative '../lib/auth'
require_relative '../lib/env'
require 'jungle/rack/basic_credentials'
require 'jungle/rack/json_body_parser'

module Web
  # web with agoo web server!!! :)
  Api = ::App::Api

  def self.run()
    Agoo::Server.init(9292, App.public_dir(), thread_count: 0)
    Agoo::Server.use(::Jungle::Rack::BasicCredentials::NoChallenge)
    Agoo::Server.use(::Jungle::Rack::JsonBodyParser)
    
    Agoo::Server.handle(:GET, "/", Api.method(:default))
    Agoo::Server.handle(:GET, "/app", Api.method(:app))
    Agoo::Server.handle(:GET, "/app/**", -> (req){
      env = Env.prep(req)
      [200, {}, ["bbb\n", env.req['PATH_INFO']]]
    })
    Agoo::Server.handle(:GET, "/contacts", Api.method(:contacts))
    
    Agoo::Server.start()
  end
end
Web.run()
