begin # requires, etc.
  # Load the 'jungle' gem as a library (not as a gem) by pushing its lib root dir to the load path:
  # This is useful when hacking on jungle ;)
  jungle_lib = ::File.expand_path('../../lib', __dir__)
  puts jungle_lib
  $LOAD_PATH.unshift(jungle_lib)

  require 'jungle'
  require 'jungle/auth/main'
  require 'jungle/auth/default/auth_provider'
  require 'jungle/auth/default/data_provider'
  require 'jungle/db/access/io/db'
  require 'jungle/db/access/meta/db'
  require 'jungle/db/access/meta/schema'
  require 'jungle/db/model/table'
  require 'jungle/schema/filter'
  require_relative '../z/zdata'
  require_relative '../config/config'
  require_relative '../lib/data_structures'
  require_relative '../schema/schema'
end

module App
  Auth = ::Jungle::Auth::Main
  puts "here!!!!!!!!!!!!!1zzz"
  AuthProvider = ::Jungle::Auth::Default::AuthProvider
  DataProvider = ::Jungle::Auth::Default::DataProvider
  Db = Jungle::Db::Access::Io::Db
  Filter = Jungle::Schema::Filter
  MetaDb = Jungle::Db::Access::Meta::Db
  MetaSchema = Jungle::Db::Access::Meta::Schema
  Model = Jungle::Db::Model::Table
  Schema = App::Schema
  ZData = App::ZData

  def self.authenticate(username, password)
    fn_auth = lambda {AuthProvider.authenticate(
      username: username,
      password: password,
      roles: roles(),
      data: {
        fn_find_user: lambda {|username| DataProvider.find_user(db(), username)},
        fn_find_roles: lambda {|user| DataProvider.find_roles(db(), user)},
      }
    )}
    identity = Auth.authenticate(fn_auth)
    #puts "identity: #{identity}."
    identity
  end

  def self.config
    @config = @config || App::Config.init()
  end

  def self.public_dir()
    @public = @public || File.expand_path('../public', __dir__)
    puts "public: #{@public}."
    @public
  end

  def self.roles
    @roles = @roles || config[:roles]
  end

  def self.logger
    #jungle.application.logger
  end

  def self.ers # entity relationships
    @ers = @ers || Model.to_ers(models)
  end

  def self.models
    @models = @models || Model.to_models_hash(Model.to_models_array(Schema.array, Schema.named_column_sets))
  end

  def self.models_alphabetical_order
    @models_alpha = @models_alpha || Model.models_alphabetical_order(models)
  end

  def self.models_dependent_order
    @models_dep = @models_dep || Model.models_dependent_order(models)
  end

  def self.controllers
    #Controllers.controllers_hash(models_alphabetical_order)
  end

  def self.db
    @db = @db || Db.new(config[:db])
  end

  def self.bootstrap!()
    MetaDb.drop?(config[:db])
    MetaDb.create(config[:db]) unless MetaDb.exists?(config[:db])
    MetaSchema.create(models_dependent_order, config[:db])
    ZData.bootstrap(db:db, models:models)
  end

  def self.schema_filter()
    puts config
    Filter.allowed(models_alphabetical_order, config[:schema_filters][:test])
  end
  
  def self.initialize!(bootstrap: false)
    #initialize...
    puts "App initializing..."
    config()
    models()
    ers()
    models_alphabetical_order()
    models_dependent_order()
    bootstrap!() if bootstrap
    #controllers()
  end
end
