require_relative '../app/app'
require 'jungle/query/engine'

module Test
  EngineTest = ::Jungle::Query::Engine::Test
  def self.query_engine(query)
    App.initialize!()
    root, values = EngineTest.parse(App.ers, query)
    show(root, 0)
    puts ""
    puts "values: #{values}"

    puts ""
    sql, va = build_sql(root, 0, [], values, [])
    puts "sql:\n#{sql}"
    puts ""
    puts sql.join("\n")
    puts ""
    puts "va: #{va}"
    nil
  end

  def self.show(e, level)
    tab = ""
    level.times{tab << '  '}
    puts "#{tab}#{e.type} #{e.table.name} #{e.aka} #{e.key} #{e.sql_alias} #{e.fields.map{|f| f.column.name}} #{e.filter} #{(e.sort||[]).map{|s| s.string_value + ' ' + s.sort}} #{e.limit} #{e.offset}"
    level += 1
    e.children.each do |child|
      show(child, level)
    end
  end

  def self.build_sql(e, level, a, values, stack)
    stack.push(e)
    a << 'select' if a.length == 0
    a << fields(e, [])
    a << from(e, [])
    wa, va = where(e, values, [], [])
    a << wa.join("\n")
    return a, va
  end
  def self.fields(e, fa)
    (e.fields||[]).each{|f| fa << "  #{e.sql_alias}.#{f.column.name}"}
    e.children.each do |child|
      fields(child, fa)
    end
    fa.join(",\n")
  end
  def self.from(e, fa)
    # check for replacement table/view/function here...
    fa << "from #{e.table.name} #{e.sql_alias}" if e.type == "!" # root table
    fa << "join #{e.table.name} #{e.sql_alias} on #{pk_fk(e)}" if e.type == "<"
    fa << "join #{e.table.name} #{e.sql_alias} on #{fk_pk(e)}" if e.type == ">"
    e.children.each do |child|
      from(child, fa)
    end
    fa.join("\n")
  end
  def self.pk_fk(c)
    # parent  <  child
    # pk      <  fk
    p = c.parent
    a = []
    i = 0
    #puts "c.aka: #{c.aka}."
    #puts "keys: #{c.table.foreign_key_columns_by_table_alias.keys}"
    c.table.foreign_key_columns_by_table_alias[c.aka].each do |k, v|
      a << "#{c.sql_alias}.#{k} = #{p.sql_alias}.#{p.table.primary_key_columns.keys[i]}"
      i += 1
    end
    a.join(" and ")
  end
  def self.fk_pk(c)
    # parent  >  child
    # fk      >  pk
    p = c.parent
    a = []
    i = 0
    c.table.primary_key_columns.each do |k, v|
      a << "#{c.sql_alias}.#{k} = #{p.sql_alias}.#{p.table.foreign_key_columns_by_table_alias[c.aka].keys[i]}"
      i += 1
    end
    a.join(" and ")
  end

  def self.where(e, values, wa, va)
    a = e.filter||[]
    a.join(" ")

    count = a.select{|v| v == "?"}.length

    remaining = nil
    if count > 0
      #puts ""
      #puts "a: #{a}."
      #puts "values: #{values}."
      va = va + values[-count..-1]
      remaining = values[0...-count]
      #puts "va: #{va}."
      #puts "re: #{remaining}."
    else
      remaining_values = values
    end

    wa << ("where " + a.join(' ')) if a.length > 0 && wa.length == 0
    wa << ("and " + a.join(' ')) if a.length > 0 && wa.length > 0

    e.children.each do |child|
      wa, va = where(child, remaining, wa, va)
    end

    return wa, va
  end

  def self.test_query_to_sql()
    query = <<~Q
    user(
      id,
      name,
      user_profile:user(
        id,
        location_id,
        location(id),
        previous_location(
          id,
          created_by_user(
            id,
            name
          )
        )
      )((location_id == 10 or location_id == 11))
    )
    (id == 1 and name == "aaa")
    (id asc, user_profile:user.location_id desc)
    [][]
    Q
    puts "input query:\n\n#{query}\n"
    query_engine(query)
  end
end
