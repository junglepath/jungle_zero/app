require 'jungle/schema/default'

module App
  module Schema
    Default = ::Jungle::Schema::Default

    def self.hash()
      h = {}
      #
      # You could do this to use the default schema from jungle_zero:
      #
      #   h = Default.hash()
      #
      # And then add in more custom tables...
      #
      # (the default schema from jungle_zero already defines 'role', 'schema_info',
      #  'user', and 'user_role' tables)
      #
      h[:account] = account
      h[:account_rollup] = account_rollup
      h[:account_rollup_z] = account_rollup_z
      h[:account_rollup_set] = account_rollup_set
      h[:address] = address
      h[:company] = company
      h[:company_address] = company_address
      h[:currency] = currency
      h[:entry] = entry
      h[:hotel] = hotel
      h[:hotel_address] = hotel_address
      h[:owner] = owner
      h[:report] = report
      h[:report_account] = report_account
      h[:role] = role
      h[:schema_info] = schema_info
      h[:transaction] = transaction
      h[:user] = user
      h[:user_company] = user_company
      h[:user_hotel] = user_hotel
      h[:user_owner] = user_owner
      h[:user_profile] = user_profile
      h[:user_role] = user_role
      h[:user_role_extended] = user_role_extended
      h
    end
    def self.named_column_sets()
      ncs = {}
      ncs[:audit_user] = {
        name: :audit_user,
        columns: [
          [:created_by_user_id, :foreign_key, [:user, :created_by_user]],
          [:created_at, :timestamp, :default, 'now()'],
          [:updated_by_user_id, :foreign_key, [:user, :updated_by_user]],
          [:updated_at, :timestamp, :default, 'now()']
        ]
      }
      ncs[:id] = {
        name: :id,
        columns: [
          [:id, :primary_key]
        ]
      }
      ncs[:ido] = {
        name: :ido,
        description: "shorthand to define an :id pk + :owner_id fk",
        columns: [
          [:id, :primary_key],
          [:owner_id, :foreign_key, :owner]
        ]
      }  
      ncs[:owner] = {
        name: :owner,
        columns: [
          [:owner_id, :foreign_key, :owner]
        ]
      }
      ncs
    end
    def self.array()
      hash.values
    end

    private

    def self.account(){
      name: :account,
      description: "chart of account accounts (a GL account}. ",
      columns: [
        [:id],
        [:name, :string],
        [:account_type_id, :foreign_key, :account_type],
        [:default_offset_account_id, :foreign_key, :account]
      ]}
    end
    def self.account_category(){
      name: :account_category,
      description: "financial, non_financial, etc",
      columns: [
        [:id],
        [:name, :string],
        [:audit_user]
      ]}
    end
    def self.account_rollup(){
      name: :account_rollup,
      description: "a rollup account",
      columns: [
        [:id, :foreign_key, :account, :primary_key]
      ]}
    end
    def self.account_rollup_z(){
      name: :account_rollup_z,
      description: "a rollup account ztest",
      columns: [
        [:id, :foreign_key, :account, :primary_key],
        [:z_account_id, :foreign_key, [:account, :z_account], :primary_key]
      ]}
    end
    def self.account_rollup_set(){
      name: :account_rollup_set,
      description: "join table for accounts in rollups",
      columns: [
        [:account_rollup_id, :foreign_key, :account_rollup, :primary_key],
        [:account_id, :foreign_key, :account, :primary_key]
      ]}
    end
    # examples of a financial accounts:
    #   Rooms Revenue - Transient
    #   Food & Beverage - Food
    #   Food & Beverage - Beverage
    #   Food & Beverage - Banquet
    #   
    def self.account_type(){
      name: :unit,
      description: "the type of account: such as revenue, expense, asset, liability, rollup, occupancy, business_source, etc.",
      columns: [
        [:id],
        [:account_category_id, :foreign_key, :account_category],
        [:audit_user]
      ]}
    end
    def self.account_unit_type(){
      name: :unit,
      description: "the type of unit that can be associated with an account, such as currency, count (of rooms, etc.), etc.",
      columns: [
        [:id],
        [:name, :string],
        [:audit_user]
      ]}
    end
    def self.address(){
      name: :address,
      description: "street or postal address",
      columns: [
        [:id, :primary_key],
        [:line1, :string],
        [:line2, :string],
        [:city, :string],
        [:state, :string],
        [:zip, :string],
        [:named_column_set, :audit_user]
      ]}
    end
    def self.company(){
      name: :company,
      description: "organization which owns one or more hotels, etc.",
      columns: [
        [:ido],
        [:name, :string],
        [:short_name, :string],
        [:division, :string],
        [:audit_user]
      ]}
    end
    def self.company_address(){
      name: :company_address,
      description: "join table",
      columns: [
        [:company_id, :foreign_key, :company, :primary_key],
        [:address_id, :foreign_key, :address, :primary_key],
        #[:type, :string, :desc, ":primary, :billing, :etc."],
        #[:name, :string],
        [:named_column_set, :audit_user]
      ]}
    end
    def self.currency(){
      name: :currency,
      description: "US Dollars, etc.",
      columns: [
        [:id, :primary_key],
        [:name, :string, :desc, "US Dollars, etc."],
        [:symbol, :string, :desc, "$, etc."],
        [:code, :string, :desc, "USD, etc."]
      ]}
    end
    def self.entry(){
      name: :entry,
      description: "portion of transaction",
      columns: [
        [:id, :primary_key],
        [:transaction_id, :foreign_key, :transaction],
        [:account_id, :foreign_key, :account],
        [:note, :string],
        [:amount, :integer]
      ]}
    end
    def self.hotel(){
      name: :hotel,
      description:  "a hotel",
      columns: [
        [:ido],
        [:name, :string],
        [:room_count, :integer],
        [:audit_user]
      ]}
    end
    def self.hotel_address(){
      name: :hotel_address,
      description: "join table",
      columns: [
        [:hotel_id, :foreign_key, :hotel, :primary_key],
        [:address_id, :foreign_key, :address, :primary_key],
        [:type, :string, :desc, ":primary, :billing, :etc."],
        [:name, :string],
        [:named_column_set, :audit_user]
      ]}
    end
    def self.owner(){
      name: :owner,
      description: "logical division of system by org/company/ownership into a mult-tenant system. the owner is one tenant.",
      columns: [
        [:id, :primary_key],
        [:name, :string],
        [:primary_contact, :string],
        [:named_column_set, :audit_user]
      ]}
    end
    def self.report(){
      name: :report,
      description: "define accounting reports",
      columns: [
        [:id, :primary_key],
        [:name, :string]
      ]}
    end
    def self.report_account(){
      name: :report_account,
      description: "list of accounts to include on report",
      columns: [
        [:report_id, :foreign_key, :report, :primary_key],
        [:sequence, :integer,:primary_key],
        [:account_id, :foreign_key, :account]
      ]}
    end
    def self.role()
      {
        name: :role,
        description: "a named security principal -- set of permissions and restrictions",
        columns: [
          [:id, :primary_key],
          [:role, :string],
          [:description, :string],
          [:enabled, :boolean],
          [:named_column_set, :audit_user]
        ],
        view: nil
      }
    end
    def self.schema_info()
      {
        name: :schema_info,
        description: "Table used by the Sequel Ruby gem to track database migration versions.",
        columns: [
          [:version, :integer, :primary_key]
        ],
        view: nil
      }
    end
    def self.transaction(){
      name: :transaction,
      description: "Set of General Ledger entries.",
      columns: [
        [:ido, :primary_key],
        [:transaction_ts, :timestamp],
        [:accounting_period, :date],
        [:description, :string],
        [:currency_id, :foreign_key, :currency]
      ]}
    end
    def self.user()
      {
        name: :user,
        description: "authenticated user of the system",
        columns: [
          [:id, :primary_key],
          [:username, :string, :unique, :not_null, :alternate_key],
          [:password_hash, :string, :not_null, :secure, :calculated, :desc, "See password_hash.rb for details."],
          [:default_role, :string, :not_null],
          [:name, :string, :desc, "Your human name! :)"],
          [:title, :string, :desc, "President / CEO/ COO / etc."],
          [:email, :string],
          [:mobile, :string],
          [:phone, :string],
          [:fax, :string],
          [:notes, :string, :desc, "For any misc. notes about this user including addition email addresses, phone numbers, etc."],
          [:enabled, :boolean, :default, false],
          [:sms_verification_code, :string, :secure, :desc, "phone verification/sign-in code."],
          [:activation_key, :string, :secure],
          [:password_reset_code, :string, :secure],
          [:named_column_set, :audit_user]
        ],
        plural: "users",
        view: nil
      }
    end
    def self.user_company(){
      name: :user_company,
      description: "associate user to company (join table)",
      columns: [
        [:user_id, :foreign_key, :user, :primary_key],
        [:company_id, :foreign_key, :company, :primary_key],
        [:audit_user]
      ]}
    end
    def self.user_hotel(){
      name: :user_hotel,
      description: "associate user to hotel (join table)",
      columns: [
        [:user_id, :foreign_key, :user, :primary_key],
        [:hotel_id, :foreign_key, :hotel, :primary_key],
        [:audit_user]
      ]}
    end
    def self.user_owner(){
      name: :user_owner,
      description: "associate user to owner (join table)",
      columns: [
        [:user_id, :foreign_key, :user, :primary_key],
        [:owner_id, :foreign_key, :owner, :primary_key],
        [:audit_user]
      ]}
    end
    def self.user_profile()
      {
        name: :user_profile,
        description: "extended user info not in main user table due to dependencies or privacy reasons.",
        columns: [
          [:id, :foreign_key, :user, :primary_key],
          [:named_column_set, :audit_user]
        ]
      }
    end
    def self.user_role()
      {
        name: :user_role,
        description: "associates a user with a role.",
        columns: [
          [:user_id, :foreign_key, :user, :primary_key],
          [:role_id, :foreign_key, :role, :primary_key],
          [:enabled, :boolean, :default, false],
          [:named_column_set, :audit_user]
        ]
      }
    end
    def self.user_role_extended()
      {
        name: :user_role_extended,
        description: "test extended role...",
        columns: [
          [:user_id, :foreign_key, :user_role, :primary_key],
          [:role_id, :foreign_key, :user_role, :primary_key],
          [:test, :string],
          [:named_column_set, :audit_user]
        ]
      }
    end
  end
end
