require 'jungle/schema/default'

module App
  module Schema
    Default = ::Jungle::Schema::Default

    def self.hash()
      h = {}
      #
      # You could do this to use the default schema from jungle_zero:
      #
      #   h = Default.hash()
      #
      # And then add in more custom tables...
      #
      # (the default schema from jungle_zero already defines 'role', 'schema_info',
      #  'user', and 'user_role' tables)
      #
      h[:location] = location
      h[:role] = role
      h[:schema_info] = schema_info
      h[:user] = user
      h[:user_profile] = user_profile
      h[:user_role] = user_role
      h[:user_role_extended] = user_role_extended
      h[:contact] = contact
      h
    end
    def self.named_column_sets()
      ncs = {}
      ncs[:audit_user] = {
        name: :audit_user,
        columns: [
          [:created_by_user_id, :foreign_key, [:user, :created_by_user]],
          [:created_at, :timestamp, :default, 'now()'],
          [:updated_by_user_id, :foreign_key, [:user, :updated_by_user]],
          [:updated_at, :timestamp, :default, 'now()']
        ]}
      ncs
    end
    def self.array()
      hash.values
    end

    private

    def self.account(){
      name: :account,
      description: "chart of account accounts (a GL account}.",
      columns: [
        [:id, :primary_key],
        [:name, :string],
        [:type, :integer, :description, "0 == revenue, 1 == expense, 2 == asset, 3 == liability, 4 == rollup"],
        [:default_offset_account_id, :foreign_key, :account]
      ]}
    end

    def self.account_rollup(){
      name: :rollup,
      description: "join table for accounts in rollups",
      columns: [
        [:id, :foreign_key, :account, :primary_key],
        [:account_id, :foreign_key, :account, :primary_key]
      ]}
    end

    def self.currency(){
      name: :currency,
      description: "US Dollars, etc.",
      columns: [
        [:id, :primary_key],
        [:name, :string, :description, "US Dollars, etc."],
        [:symbol, :string, :description, "$, etc."],
        [:code, :string, :description, "USD, etc."]
      ]}
    end

    def self.entry(){
      name: :entry,
      description: "portion of transaction",
      columns: [
        [:id, :primary_key],
        [:transaction_id, :foreign_key, :transaction],
        [:account_id, :foreign_key, :account],
        [:note, :string],
        [:amount, :integer]
      ]}
    end

    def self.transaction(){
      name: :transaction,
      description: "Set of General Ledger entries.",
      columns: [
        [:id, :primary_key],
        [:account_id, :foreign_key, :account],
        [:transaction_ts, :timestamp],
        [:accounting_period, :date],
        [:description :string],
        [:currency_id, :foreign_key, :currency],
        [:tag_set_id, :foreign_key, :tag_set]
      ]}
    end

    def self.tag_set(){
        name: :tag_set,
        description: "set of tags associated with some entity",
        columns: [
          [:id, :primary_key]
        ]}
    end

    def self.tag_set_tag(){
        name: :tag_set_tag,
        description: "tag in tag_set",
        columns: [
          [:tag_set_id, :foreign_key, :tag_set, :primary_key],
          [:tag_id, :foreign_key, :tag, :primary_key]
        ]}
    end

    def self.tag(){
      name: :tag,
      description: "a tag",
      columns: [
        [:id, :primary_key],
        [:tag_type, :string, :description, ":tag, :property, :etc"]
      ]}
    end

    def self.tag_tag(){
      name: :tag_tag,
      description: "a tag tag (a plain old tag), but can be categorized",
      columns: [
        [:id, :foreign_key, :tag, :primary_key],
        [:category, :string, :description, "room_amenities, hotel_amenities, etc."],
        [:tag, :string]
      ]}
    end

    def self.tag_property(){
      name: :tag_property,
      description: "a tag property (a tag identifying a property)",
      columns: [
        [:id, :foreign_key, :tag, :primary_key],
        [:property_id, :foreign_key, :property]
      ]}
    end

    def self.report(){
      name: :report,
      description: "define accounting reports",
      columns: [
        [:id, :primary_key],
        [:name, :string]
      ]}
    end

    def self.report_account(){
      name: :report_account,
      description: "list of accounts to include on report",
      columns: [
        [:report_id, :foreign_key, :report, :primary_key],
        [:sequence, :integer,:primary_key]
        [:account_id, :foreign_key, :account]
      ]}
    end

    def self.organization(){
      name: :organization,
      description: "organization which owns one or more hotels, etc.",
      columns: [
        [:id, :primary_key],
        [:name, :string],
        [:named_column_set, :audit_user]
      ]}
    end

    def self.organization_profile(){
      name: :organization_profile,
      description: "organization extended information",
      columns: [
        [:id, :foreign_key, :organization, :primary_key],
        [:name, :string],
        [:named_column_set, :audit_user]
      ]}
    end

    def self.location()
      {
        name: :location,
        description: "where are you? here!",
        columns: [
          [:id, :primary_key],
          [:lat, :float],
          [:lon, :float],
          [:named_column_set, :audit_user]
        ]
      }
    end
    
    def self.property(){
      name: :property,
      description:  "a hotel, unit, etc.",
      columns: [
        [:id, :primary_key],
        [:parent_id, :foreign_key, :property],
        [:name, :string],
        [:property_type_id, :foreign_key, :property_type],
        [:owner_id, :foreign_key, :owner],
        [:named_column_set, :audit_user]
      ]}
    end

    def self.property_type(){
      name: :property_type,
      description:  "resort, development, hotel, building, unit, condo, apartment, house, townhouse, ...",
      columns: [
        [:id, :primary_key],
        [:name, :string],
        [:code, :string]
      ]}
    end

    def self.resident(){
      name: :resident,
      description: "someone who resides at a property (owner, tenant, etc.)"
      columns: [
        [:id, :primary_key],
        [:person_id, :foreign_key, :person],
        [:property_id, :foreign_key, :property]
      ]}
    end

    def self.person(){
      name: :person,
      description: "a human",
      columns: [
        [:id, :primary_key],
        [:name, :string],
        [:note, :string]
      ]}
    end

    def self.person_contact(){
      name: :person_contact,
      description: "way to contact this person, phone number, email, etc.",
      columns: [
        [:id, :primary_key],
        [:label, :string],
        [:data, :string],
        [:preferred, :boolean],
        [:contact_type_id, :foreign_key, :contact_type_id]
      ]}
    end

    def self.contact_type(){
      name: :person_contact,
      description: "mobile, landline, phone, email, carrier_pigeon, etc.",
      columns: [
        [:id, :primary_key],
        [:description, :string],
        [:code, :string]
      ]}
    end

    def self.property_location(){
      name: :address,
      description: "street address / unit number / etc.",
      columns: [
        [:id, :primary_key],
        [:property_id, :foreign_key, :property],
        [:text, :string],
      ]}
    end

    def self.role()
      {
        name: :role,
        description: "a named security principal -- set of permissions and restrictions",
        columns: [
          [:id, :primary_key],
          [:role, :string],
          [:description, :string],
          [:enabled, :boolean],
          [:named_column_set, :audit_user]
        ],
        view: nil
      }
    end

    def self.schema_info()
      {
        name: :schema_info,
        description: "Table used by the Sequel Ruby gem to track database migration versions.",
        columns: [
          [:version, :integer, :primary_key]
        ],
        view: nil
      }
    end

    def self.user()
      {
        name: :user,
        description: "authenticated user of the system",
        columns: [
          [:id, :primary_key],
          [:username, :string, :unique, :not_null, :alternate_key],
          [:password_hash, :string, :not_null, :secure, :calculated, :desc, "See password_hash.rb for details."],
          [:default_role, :string, :not_null],
          [:name, :string, :desc, "Your human name! :)"],
          [:email, :string],
          [:mobile, :string],
          [:organization, :string, :secure],
          [:notes, :string, :desc, "For any misc. notes about this user including addition email addresses, phone numbers, etc."],
          [:enabled, :boolean, :default, false],
          [:sms_verification_code, :string, :secure, :desc, "phone verification/sign-in code."],
          [:activation_key, :string, :secure],
          [:password_reset_code, :string, :secure],
          [:named_column_set, :audit_user]
        ],
        plural: "users",
        view: nil
      }
    end

    def self.user_profile()
      {
        name: :user_profile,
        description: "extended user info not in main user table due to dependencies or privacy reasons.",
        columns: [
          [:id, :foreign_key, :user, :primary_key],
          [:location_id, :foreign_key, :location],
          [:previous_location_id, :foreign_key, [:location, :previous_location]],
          [:named_column_set, :audit_user]
        ]
      }
    end

    def self.user_role()
      {
        name: :user_role,
        description: "associates a user with a role.",
        columns: [
          [:user_id, :foreign_key, :user, :primary_key],
          [:role_id, :foreign_key, :role, :primary_key],
          [:enabled, :boolean, :default, false],
          [:named_column_set, :audit_user]
        ]
      }
    end

    def self.user_role_extended()
      {
        name: :user_role_extended,
        description: "associates a user with a role.",
        columns: [
          [:user_id, :foreign_key, :user_role, :primary_key],
          [:role_id, :foreign_key, :user_role, :primary_key],
          [:test, :string],
          [:named_column_set, :audit_user]
        ]
      }
    end

    def self.contact()
      {
        name: :contact,
        description: "your contacts",
        columns: [
          [:id, :primary_key],
          [:name, :string],
          [:phone, :string]
        ]
      }
    end
  end
end
