# base_config.rb
require 'date'
require 'jungle/config'

# create a override.rb to override any of the jungle... values as needed:
module App
	module Config
		def self.init
			puts "initializing app config..."
			hash = {}
			hash = on_startup(hash)
			hash = db(hash)
			hash = password_settings(hash)
			hash = roles(hash)
			hash = schema_filters(hash)
			hash = role_schema_filters(hash)
			hash = role_table_filters(hash)
			hash = route_access(hash)
			hash = debug(hash)
			hash = Jungle::Config.override_settings(__dir__, hash, self, name: 'override')
			#jungle.lock = true
			hash
		end

		private

		def self.on_startup(hash)
			hash[:on_startup] = {
				run_database_migrations: false # Servers usually handle this on their own with deployments. For devs, may want to override in override.rb.
			}
			hash
		end

		def self.db(hash)
			hash[:db] = {
				name: 'jungle_zero_app',
				type: 'postgres',
				user_name: 'jq',
				password: nil,
				host: 'localhost',
				extensions: [:pg_json],
				port: nil, # defaults to PostgreSQL default port of 5432 if nil.
				options: {max_connections: 4}
			}
			hash
		end

		def self.password_settings(hash)
			hash[:password_settings] = {
				length: {must_be_greater_than: 7, message: "Password length must be at least 8 characters."},
				#length: {must_be_greater_than: 7, message: "Password length must be at least 8 characters."},
				regular_expression_matches: [
					## {expression: /[[:alpha:]]/, message: "Password must have at least one alphabetical character."},
					## {expression: /[[:digit:]]/, message: "Password must have at least one numeric character."}
					#{expression: /\D/, message: "Password must have at least one alphabetical character."},
					#{expression: /\d/, message: "Password must have at least one numeric character."}
				]
			}
			hash
		end

		def self.roles(hash)
			hash[:roles] = {
				root: {
					id: 0,
					name: :root,
					description: 'root can do anything',
					permissions: [:root],
					restrictions: []
				},
				admin: {
					id: 1,
					name: :admin,
					description: 'admin and add, edit and delete users, but not root users or other admin users.',
					permissions: [:write, :read, :admin, :auth, :assume_user_identity],
					restrictions: []
				},
				user: {
					id: 2,
					name: :user,
					description: 'basic system user -- limited access.',
					permissions: [:write, :read, :assumable_user_identity, :user],
					#restrictions: [:query_only, :user, :me_related]
					restrictions: [:write_own_practice]
				}
			}
			hash
		end

		def self.schema_filters(hash)
			hash[:schema_filters] = {
				allow_all_tables: {allow: [table: /./]},
				hide_nonpublic_tables: {allow: [{table: /./}], deny: [{table: /^temp_/}]},
				test: {
					allow: [
						{table: :user, columns: [:id, :username, :name, :location_id, :previous_location_id]},
						{table: :role}
					],
					deny: [
						{table: :role, columns: [:enabled]}
					]
				}
			}
			hash
		end

		def self.role_schema_filters(hash)
			hash[:role_schema_filters] = {
				root: :allow_all_tables,
				admin: :allow_all_tables,
				user: :hide_nonpublic_tables
			}
			hash
		end

		def self.role_table_filters(hash)
			# Replace usage of a table in a query with the given view or parameterized function.
			# filter_? tables are included just in case user queries them directly. This will force
			# correct parameters are included.
			# Similar idea for the my_* tables as well -- these should be used directly.
			hash[:role_table_filters] = {
				root: [
					{table_name: :user, replacement: :user},
					{table_name: :filter_user, replacement: :user},
					{table_name: :group, replacement: :group},
					{table_name: :group_available_member, replacement: :group_available_member},
					{table_name: :practice, replacement: :practice_view},
					{table_name: :quiz_available_question, replacement: :quiz_available_question},
					{table_name: :filter_quiz_available_question, replacement: :filter_quiz_available_question},
					{table_name: :filter_group, replacement: :group},
					{table_name: :filter_question, replacement: :question},
					{table_name: :filter_quiz, replacement: :quiz},
					{table_name: :filter_practice, replacement: :practice_view}
				],
				admin: [
					{table_name: :user, replacement: :user},
					{table_name: :filter_user, replacement: :user},
					{table_name: :group, replacement: :group},
					{table_name: :group_available_member, replacement: :group_available_member},
					{table_name: :practice, replacement: :practice_view},
					{table_name: :quiz_available_question, replacement: :quiz_available_question},
					{table_name: :filter_quiz_available_question, replacement: :filter_quiz_available_question},
					{table_name: :filter_group, replacement: :group},
					{table_name: :filter_question, replacement: :question},
					{table_name: :filter_quiz, replacement: :quiz},
					{table_name: :filter_practice, replacement: :practice_view}
				],
				user: [
					{table_name: :user, replacement: :filter_user},
					{table_name: :filter_user, replacement: :filter_user},
					{table_name: :group, replacement: :filter_group},
					{table_name: :filter_group, replacement: :filter_group},
					{table_name: :group_available_member, replacement: :filter_group_available_member},
					{table_name: :filter_group_available_member, replacement: :filter_group_available_member},
					{table_name: :question, replacement: :filter_question},
					{table_name: :filter_question, replacement: :filter_question},
					{table_name: :quiz, replacement: :filter_quiz},
					{table_name: :quiz_available_question, replacement: :filter_quiz_available_question},
					{table_name: :filter_quiz_available_question, replacement: :filter_quiz_available_question},
					{table_name: :filter_quiz, replacement: :filter_quiz},
					{table_name: :practice, replacement: :filter_practice},
					{table_name: :practice_view, replacement: :filter_practice},
					{table_name: :filter_practice, replacement: :filter_practice}
				]
			}
			hash
		end

		def self.route_access(hash)
			hash[:route_access] = {
				# any user (authenticated or not) can access 'public' paths.
				public: {
					get: {
						routes: ['/'],
						routes_start_with: ['/activate/', '/passwordresetcode/', '/img/', '/js/', '/css/', '/evenorodd/']
					}
				},
				# any authenticated user can access 'authenticated' paths regardless of roles/permissions/restrictions.
				authenticated: {
					get: {
						routes: ['/query/schema_tree'],
						routes_start_with: []
					},
					post: {
						routes: ['/practice'],
						routes_start_with: []
					},
					put: {
						routes: [],
						routes_start_with: ['/practice/']
					}
				}
			}
			hash
		end

		def self.debug hash
			hash[:debug] = {
				show_params: false
			}
			hash
		end
	end
end
